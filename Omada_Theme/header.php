<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<link rel="stylesheet" href="http://agilesolutionspk.com/qa13/wp-content/themes/Omada_Theme/style.css" type="text/css" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="container"> 
	<div class="row" id="header"> 
		<div class="col-md-12">
			<div class="row" id="top-header"> 
				<div class="col-md-12"> </div>
			</div>
			<div class="row" id="home-slider"> 
				<div class="col-md-12"> </div>
			</div>
		</div>
	</div>
</div>
		</header><!-- .site-header -->

		<div id="content" class="site-content">
