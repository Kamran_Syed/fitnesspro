<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>FitnessPro | HTML</title>
        <!-- Bootstrap -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Theme CSS Area -->
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/color.css">
        <link rel="stylesheet" href="assets/css/css-menu/css-menu.css">
        <link rel="stylesheet" href="assets/css/custom-responsive.css">
        <link rel="stylesheet" href="assets/css/responsive.css">
        <link rel="stylesheet" href="assets/css/hover.css">
        <!-- Fraction Slider CSS Area -->
        <link rel="stylesheet" href="assets/css/fraction-slide/slider-style.css" type="text/css" />
        <link rel="stylesheet" href="assets/css/fraction-slide/fractionslider.css" type="text/css" />
        <!-- Owl Carosel CSS Area -->
        <link rel="stylesheet" href="assets/css/owl-carousel/owl.carousel.css" type="text/css" />
        <link rel="stylesheet" href="assets/css/owl-carousel/owl.theme.css" type="text/css" />
        <!-- FontAwesome CSS Area -->
        <link href="assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body> 
        <div class="wrapper ft-main-content">
            <header>
                <div class="section-header navbar-fixed-top">
                    <div class="header-serarch-area">
                        <div class="container">
                            <div class="pull-right search  col-lg-3 col-md-4 col-sm-5 col-xs-12">
                                <form method="post">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search for...">
                                        <span class="input-group-btn"><button class="btn btn-default" type="button">
                                                <i class="fa fa-search"></i>
                                            </button></span>
                                    </div>
                                    <!-- /input-group -->
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="header">
                        <div class="container"> 
                            <div class="row">
                                <!-- Logo Area -->
                                <div class="logo-responsive col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <div class="ft-logo">
                                        <a href="index.html">
                                            <img src="assets/images/omada/OmadaLogo.png" alt="Omada Logo">
                                        </a>
                                    </div>
                                </div>
                                <!-- Navigation Area -->
                                <div class="menu-responsive col-lg-7 col-md-7 col-sm-7 col-xs-12 pull-right">
                                    <div class="ft-menu">
                                        <nav id="cssmenu" class="nav">
                                            <ul class="pull-right">
                                                <li class="active">
                                                    <a href="#home">Home</a>
                                                </li>
                                                <li>
                                                    <a href="#about-us">About Us</a>
                                                </li>
                                                <li>
                                                    <a href='#services'>Services</a>
                                                </li>
                                                <li>
                                                    <a href='#testimonials'>Testimonials</a>
                                                </li>
                                                <li>
                                                    <a href="blog.html">Blog</a>
                                                </li>
                                                <li>
                                                    <a href="#contact-us">Contact Us</a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                                <!-- Search & Cart Area -->
                            </div>                             
                        </div>                         
                    </div>
                </div>
                <div class="home-header-warpper home-page" id="home">
                    <!-- ***************************************************************************************************************** -->
                    <!-- *********************************************** Home Slider Area ************************************************ -->
                    <!-- ***************************************************************************************************************** -->
                    <section class="rev_slider_wrapper gardener-banner">
                        <div id="slider1" class="rev_slider" data-version="5.0">
                            <ul>
                                <li data-transition="parallaxvertical">
                                    <img src="assets/images/omada/HomeBanner1.jpg" alt="" width="1024" height="669" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1">
                                    <div class="tp-caption sfl tp-resizeme slider-top-title" data-x="left" data-hoffset="0" data-y="top" data-voffset="380" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="500">
                                        Functional Movement
</div>
                                    <div class="tp-caption sfr tp-resizeme slider-bottom-title" data-x="left" data-hoffset="0" data-y="top" data-voffset="460" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="1000">
                                        And Livestyle Coaching
</div>
                                </li>
                                <li data-transition="parallaxvertical">
                                    <img src="assets/images/omada/HomeBanner3.jpg" alt="" width="1920" height="705" data-bgposition="center right" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="3">
                                    <div class="tp-caption sfl tp-resizeme slider-top-title yellow-color" data-x="right" data-hoffset="0" data-y="top" data-voffset="200" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="500">
                                        Join Our Team Of
</div>
                                    <div class="tp-caption sfr tp-resizeme slider-bottom-title" data-x="right" data-hoffset="0" data-y="top" data-voffset="280" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="1000">
                                        Active Women
</div>
                                </li>
                                <li data-transition="parallaxvertical">
                                    <img src="assets/images/omada/HomeBanner2.jpg" alt="" width="1920" height="705" data-bgposition="top center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="2">
                                    <div class="tp-caption sfl tp-resizeme slider-top-title" data-x="left" data-hoffset="0" data-y="top" data-voffset="380" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="500">
                                        The Time Is Now
</div>
                                    <div class="tp-caption sfr tp-resizeme slider-bottom-title" data-x="left" data-hoffset="0" data-y="top" data-voffset="460" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="o:0" data-transform_out="o:0" data-start="1000">
                                        To Make A Change
</div>
                                </li>
                            </ul>
                        </div>
                    </section>
                    <div class="ft-header-bottom-fitness-level-title">
                        <p>Training By Kate Gibson</p>
                    </div>
                </div>
            </header>
            <main wp-site-content>
                <div class="ft-newsletter-area">
                    <div class="container">
                        <div class="row">
                            <div class="newsletter-text col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="ft-newsletter-des">
                                    <h4>Join Our Mailing List</h4>
                                    <p>Get Regular Updates Straight To your Inbox</p>
                                </div>
                            </div>
                            <div class="newsletter-content col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <form method="post">
                                    <div class="ft-newletter-form-wrap">
                                        <div class="row">
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <div class="ft-newletter-form">
                                                    <input type="email" name="newletter" id="newletter" class="newletter form-control" placeholder="Enter your email address" required>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                <div class="ft-newletter-button">
                                                    <button type="submit" class="btn btn-default">
                                                        <span class="text">Send</span>
                                                        <span class="icon"><i class="fa fa-arrow-circle-right"></i></span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ft-main-content">
                    <!-- ***************************************************************************************************************** -->
                    <!-- ********************************************* Home Getting Started Area ***************************************** -->
                    <!-- ***************************************************************************************************************** -->
                    <!-- ***************************************************************************************************************** -->
                    <!-- ******************************************** Home Fitness Program Area ***************************************** -->
                    <!-- ***************************************************************************************************************** -->
                    <!-- ***************************************************************************************************************** -->
                    <!-- *************************************** Home Fitness Awesome Classes Area *************************************** -->
                    <!-- ***************************************************************************************************************** -->
                    <!-- ***************************************************************************************************************** -->
                    <!-- ******************************************* Home Fitness Discount Area ******************************************* -->
                    <!-- ***************************************************************************************************************** -->
                    <!-- ***************************************************************************************************************** -->
                    <!-- ******************************************* Home Fitness Courses Area ******************************************* -->
                    <!-- ***************************************************************************************************************** -->
                    <!-- ***************************************************************************************************************** -->
                    <!-- ************************************************** Home Team Area *********************************************** -->
                    <!-- ***************************************************************************************************************** -->
                    <!-- ***************************************************************************************************************** -->
                    <!-- ********************************************** Home Testimonial Area ******************************************** -->
                    <!-- ***************************************************************************************************************** -->
                    <!-- ***************************************************************************************************************** -->
                    <!-- ********************************************** Home Price Table Area ******************************************** -->
                    <!-- ***************************************************************************************************************** -->
                </div>
                <div class="ft-fitness-program" id="about-us">
                    <div class="container">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                            <div class="fintess-program-img">
                                <div class="text wow fadeIn" data-wow-delay="0.5s">
                                    <p>Change The Way You Train</p>
                                </div>
                                <div class="image-warpper wow fadeIn" data-wow-delay="0.5s">
                                    <div class="img-1-warapper">
                                        <div class="img-1">
                                            <img src="assets/images/omada/DifferenceA.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="img-2-warapper wow fadeIn" data-wow-delay="0.5s">
                                        <div class="img-2">
                                            <img src="assets/images/omada/DifferenceB.jpg" alt="ft-program-image-02">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <div class="fintess-program-tab-area wow fadeIn" data-wow-delay="0.5">
                                <div class="tab-content">
                                    <div class="ft-section-title-wrapper">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <div class="ft-section-title">
                                                    <h2><span class="title">A Different Approach</span></h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="lead">At OMADA, we are not here to lift static and structured weights. We focus on a very fun and playful workout environment that caters to all levels of fitness. This non-threatening and very easy to adapt training environment means you will be pushed without being broken or ‘destroyed’. You will find the fun and enjoyment in exercise, and we find our members like to stick around! </p>
                                    <p class="lead">We believe in enjoying movement for life. When you are stressed or angry, your muscles and tissues hold onto this and will not move as freely as when you are in a happy and positive mindset.  Our goal is to create this happiness and enjoyment aspect of life in your training sessions through a culture of game based training, functional and freeing movement, and through training in teams. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ft-discount-area">
                    <div class="ft-discount col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="ft-discount-image">
                            <img class="img-responsive" src="assets/images/omada/KateGibson.jpg" alt="discount-img">
                        </div>
                    </div>
                    <div class="ft-discount col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="ft-discount-content-wrapper ">
                            <div>
                                <h4 class="text-uppercase" style="padding-bottom: 20px;">Where Does Omada Active Come From?</h4>
                                <p class="lead">Omada Active stands for creating an active lifestyle through health, fitness, and wellbeing in a community of supportive and likeminded women. Derived from the greek word for "team", Omada is the backbone, of what we believe, is the most effective way to achieve results in your health and fitness. This is established through being a part of a team that will support, encourage, educate and believe in you! Join us and our Team of Active Women!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ft-section-feature-class-about" data-pgc="om.cta" id="services">
                    <!-- Section Title Starting Point -->
                    <div class="ft-section-title-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="ft-section-title">
                                        <h4 class="text-center"><span>Women Only Training With A Difference</span></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Section Title Ending Point -->
                    <div class="ft-feature-class-about">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="feature-class-about">
                                        <div class="image img-circle">
                                            <img src="assets/images/omada/Functional.png" width="64" height="64" alt="class-icon">
                                        </div>
                                        <div class="content">
                                            <h6>Functional Movement Training</h6>
                                            <p>Learn the best and most important tools to move your body for the challenges that life throws your way. Our workouts focus on using functional training & equipment.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="feature-class-about">
                                        <div class="image img-circle">
                                            <img src="assets/images/omada/Natal.png" width="64" height="64" alt="class-icon">
                                        </div>
                                        <div class="content">
                                            <h6>Pre & Post Natal Training</h6>
                                            <p>Know that you can train and stay active and healthy through your pregnancy and afterwards with our qualified coaches. Keep baby and mum on the right track through from conception to delivery and beyond.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="feature-class-about">
                                        <div class="image img-circle img-circle">
                                            <img src="assets/images/omada/Group.png" width="64" height="64" alt="class-icon">
                                        </div>
                                        <div class="content">
                                            <h6>Semi Private & Group</h6>
                                            <p>We also offer the opportunity to train and move with like-minded women! Enjoy your workouts in small groups or with your partner and see how beneficial this training technique can be to your results.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="feature-class-about img-circle">
                                        <div class="image img-circle">
                                            <img src="assets/images/omada/Rehab.png" width="64" height="64" alt="class-icon">
                                        </div>
                                        <div class="content">
                                            <h6>Rehab Training</h6>
                                            <p>Have you had surgery or injuries and just want to trust that your trainer knows how to cater to your individual needs? Our rehab certified trainers can look after you the way you deserve.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ft-courses-area-warp">
                    <div class="ft-courses-area">
                        <!-- Section Title Starting Point -->
                        <div class="ft-section-title-wrapper">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="ft-section-title">
                                            <h4 class="text-center"><span>Our Packages</span></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Section Title Ending Point -->
                        <!-- Section Courses Main Content Starting Point -->
                        <div class="ft-courses-main-content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="ft-courses">
                                            <div class="left ft-courses-hover-area">
                                                <img src="assets/images/omada/Package1.jpg" alt="Package1">
                                            </div>
                                            <div class="right">
                                                <div class="left">
                                                    <h4>6</h4>
                                                    <span>Weeks</span>
                                                </div>
                                                <div class="right">
                                                    <h4>Introductory</h4>
                                                    <p>A short introduction to the world of health and fitness, this package is ideal for women looking for a taste of what can be achieved.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="ft-courses">
                                            <div class="left ft-courses-hover-area">
                                                <img src="assets/images/omada/Package2.jpg" alt="Package2">
                                            </div>
                                            <div class="right">
                                                <div class="left">
                                                    <h4>12</h4>
                                                    <span>Weeks</span>
                                                </div>
                                                <div class="right">
                                                    <h4>Kickstarter</h4>
                                                    <p>Ideal for women who are new to training and want to kick start their fitness journey. </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="ft-courses">
                                            <div class="left ft-courses-hover-area">
                                                <img src="assets/images/omada/Package3.jpg" alt="Package3">
                                            </div>
                                            <div class="right">
                                                <div class="left">
                                                    <h4>12</h4>
                                                    <span>Weeks</span>
                                                </div>
                                                <div class="right">
                                                    <h4>Trim And Tone</h4>
                                                    <p>Suited to women with an established fitness routine, this package focusses on sculpting and trimming.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="ft-courses">
                                            <div class="left ft-courses-hover-area">
                                                <img src="assets/images/omada/Package4.jpg" alt="Package4">
                                            </div>
                                            <div class="right">
                                                <div class="left">
                                                    <h4>18</h4>
                                                    <span>Weeks</span>
                                                </div>
                                                <div class="right">
                                                    <h4>Fat Blaster</h4>
                                                    <p>The ultimate package for any fitness level, this package is ideal for women wanting to transform their health and fitness.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Section Courses Main Content Ending Point -->
                        <!-- Section Courses Time Line Starting Point -->
                        <!-- Section Courses Time Line Starting Point -->
                    </div>
                </div>
                <div class="ft-class-isotop-section ft-gallery-v2" data-pgc="om.gal">
                    <div class="ft-section-title-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="ft-section-title">
                                        <h4 class="text-center"><span>Join Team Omada</span></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row1">
                        <div id="grid">
                            <div class="ft-item mix building col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <a href="assets/images/omada/Gallery1.jpg" class="fancybox ft-class-item">
                                    <div class="image-wrap">
                                        <div class="image">
                                            <img src="assets/images/omada/Gallery1.jpg" alt="Gallery1">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="ft-item mix boxing fitness col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <a href="assets/images/omada/Gallery2.jpg" class="fancybox ft-class-item">
                                    <div class="image-wrap">
                                        <div class="image">
                                            <img src="assets/images/omada/Gallery2.jpg" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="ft-item mix yogs fitness col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <a href="assets/images/omada/Gallery3.jpg" class="fancybox ft-class-item">
                                    <div class="image-wrap">
                                        <div class="image">
                                            <img src="assets/images/omada/Gallery3.jpg" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="ft-item mix medication boxing col-lg-3 col-md-3 col-sm-3 col-xs-12" style="padding-bottom: 0px;">
                                <a href="assets/images/omada/Gallery4.jpg" class="fancybox ft-class-item">
                                    <div class="image-wrap">
                                        <div class="image">
                                            <img src="assets/images/omada/Gallery4.jpg" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="ft-item mix fitness col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <a href="assets/images/omada/Gallery5.jpg" class="fancybox ft-class-item">
                                    <div class="image-wrap">
                                        <div class="image">
                                            <img src="assets/images/omada/Gallery5.jpg" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="ft-item mix martial col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <a href="assets/images/omada/Gallery6.jpg" class="fancybox ft-class-item">
                                    <div class="image-wrap">
                                        <div class="image">
                                            <img src="assets/images/omada/Gallery6.jpg" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="ft-item mix yogs col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <a href="assets/images/omada/Gallery7.jpg" class="fancybox ft-class-item">
                                    <div class="image-wrap">
                                        <div class="image">
                                            <img src="assets/images/omada/Gallery7.jpg" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="ft-item mix martial yogs col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <a href="assets/images/omada/Gallery8.jpg" class="fancybox ft-class-item">
                                    <div class="image-wrap">
                                        <div class="image">
                                            <img src="assets/images/omada/Gallery8.jpg" alt="">
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ft-why-choose-us" data-pgc="om.cta2">
                    <div class="container">
                        <div class="col-lg-3 col-md-3 col-xs-12">
</div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <!-- Section Title Starting Point -->
                            <div class="ft-section-title-wrapper">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="ft-section-title">
                                            <h4><span class="title">How We Train</span></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Section Title Ending Point -->
                            <div class="why-choose-us-content">
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="why-choose-us">
                                            <div class="icon">
                                                <div class="icon-image img-circle">
                                                    <img src="assets/images/omada/Time.png" alt="">
                                                </div>
                                            </div>
                                            <div class="content">
                                                <h6>Short & Sweet</h6>
                                                <p>We have a method to our madness! We coach in 30 minute sessions.  This is widely researched around the world, to be the most effective training method to burn more fat in shorter bursts at higher intervals than steady state cardio.  Some studies have found that intervals can be up to 9 times more effective than the longer training options.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <div class="why-choose-us">
                                            <div class="icon">
                                                <div class="icon-image img-circle">
                                                    <img src="assets/images/omada/Weights.png" alt="">
                                                </div>
                                            </div>
                                            <div class="content">
                                                <h6>Function First</h6>
                                                <p>All of our sessions as based on the functional movement required for daily life. Although we use some equipment, we focus on multi-plane movement in which the various supporting muscles are recruited to help do the work too. There are not traditional weight lifting programs prescribed, focusing on single muscle use (Such as a bicep, a tricep etc.). Functional training will improve your coordination, core strength, balance, and overall body movement and function.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ft-testimonial parallax" id="testimonials">
                    <div class="container">
                        <div id="carousel-ft-testimonial" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">
                                <div class="row item active">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="images-wrap">
                                            <div class="image img-circle">
                                                <img class="img-circle" src="assets/images/omada/Lara.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <div class="content-wrap">
                                            <div class="content">
                                                <div class="blq-icon">
                                                    <i class="fa fa-quote-left"></i>
                                                </div>
                                                <div class="blq-text">
                                                    <p>Best trainer I ever had, Omada Active helped me to stay in focus encouraging me to achieve my goals through really good quality movement.</p>
                                                </div>
                                            </div>
                                            <div class="user">
                                                <p>Lara W.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row item">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="images-wrap">
                                            <div class="image img-circle">
                                                <img class="img-circle" src="assets/images/omada/Tricia.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <div class="content-wrap">
                                            <div class="content">
                                                <div class="blq-icon">
                                                    <i class="fa fa-quote-left"></i>
                                                </div>
                                                <div class="blq-text">
                                                    <p>As a woman in her mid forties who up until 10 weeks ago hadn't even stepped into a gym, I am amazed every day at the improvements already in my fitness and body shape. The support, encouragement and knowledge I have received from Kate at Omada Active has been nothing short of life changing.</p>
                                                </div>
                                            </div>
                                            <div class="user">
                                                <p>- Tricia D.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row item">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div class="images-wrap">
                                            <div class="image img-circle">
                                                <img class="img-circle" src="assets/images/omada/Jess.jpg" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                        <div class="content-wrap">
                                            <div class="content">
                                                <div class="blq-icon">
                                                    <i class="fa fa-quote-left"></i>
                                                </div>
                                                <div class="blq-text">
                                                    <p>I had never joined a gym in my life, I've always been a big girl but Kate made me feel so welcome as a newcomer. Kate always pushes me and understands my limits and believes that I can achieve anything. Her dedication and encouragement provides the support I need to achieve my goals. Simply the best.</p>
                                                </div>
                                            </div>
                                            <div class="user">
                                                <p>- Jess M.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                         
                        <!-- Controls -->
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <div class="ft-testimonial-control-wrap">
                                    <div class="ft-carousel-control">
                                        <a class="left carousel-control" href="#carousel-ft-testimonial" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                                        <a class="right carousel-control" href="#carousel-ft-testimonial" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12"></div>
                        </div>                         
                    </div>
                </div>
                <div class="ft-blog" id="blog">
                    <!-- Section Title Starting Point -->
                    <div class="ft-section-title-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="ft-section-title">
                                        <h4><span class="title">Omada Transformations</span></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Section Title Ending Point -->
                    <!-- Blog Content Section Starting Point -->
                    <div class="ft-recent-post">
                        <div class="container">
                            <div class="row" style="margin-bottom: 25px;">
                                <div class="col-sm-12 col-xs-12 col-md-3 col-lg-3">
                                    <div class="single-post">
                                        <div class="image">
                                            <img class="img-responsive" src="assets/images/omada/TriciaBefore.jpg" alt="Tricia Before">
                                        </div>
                                        <div class="content" style="bottom: 0px;">
                                            <h4>Before</h4>
                                            <div class="content-passage">
</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-md-3 col-lg-3">
                                    <div class="single-post">
                                        <div class="image">
                                            <img class="img-responsive" src="assets/images/omada/TriciaAfter.jpg" alt="Home Single Post">
                                        </div>
                                        <div class="content" style="bottom: 0px;">
                                            <h4>After</h4>
                                            <div class="content-passage">
</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-md-3 col-lg-3">
                                    <div class="single-post">
                                        <div class="image">
                                            <img class="img-responsive" src="assets/images/omada/JessBefore.jpg" alt="Jess Before">
                                        </div>
                                        <div class="content" style="bottom: 0px;">
                                            <h4>Before</h4>
                                            <div class="content-passage">
</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-md-3 col-lg-3">
                                    <div class="single-post">
                                        <div class="image">
                                            <img class="img-responsive" src="assets/images/omada/JessAfter.jpg" alt="Jess After">
                                        </div>
                                        <div class="content" style="bottom: 0px;">
                                            <h4>After</h4>
                                            <div class="content-passage">
</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-bottom: 25px;">
                                <div class="col-sm-12 col-xs-12 col-md-3 col-lg-3">
                                    <div class="single-post">
                                        <div class="image">
                                            <img class="img-responsive" src="assets/images/omada/MollieBefore.jpg" alt="Mollie Before">
                                        </div>
                                        <div class="content" style="bottom: 0px;">
                                            <h4>Before</h4>
                                            <div class="content-passage">
</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-md-3 col-lg-3">
                                    <div class="single-post">
                                        <div class="image">
                                            <img class="img-responsive" src="assets/images/omada/MollieAfter.jpg" alt="Mollie After">
                                        </div>
                                        <div class="content" style="bottom: 0px;">
                                            <h4>After</h4>
                                            <div class="content-passage">
</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-md-3 col-lg-3">
                                    <div class="single-post">
                                        <div class="image">
                                            <img class="img-responsive" src="assets/images/omada/IvyBefore.jpg" alt="Ivy Before">
                                        </div>
                                        <div class="content" style="bottom: 0px;">
                                            <h4>Before</h4>
                                            <div class="content-passage">
</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-xs-12 col-md-3 col-lg-3">
                                    <div class="single-post">
                                        <div class="image">
                                            <img class="img-responsive" src="assets/images/omada/IvyAfter.jpg" alt="Ivy After">
                                        </div>
                                        <div class="content" style="bottom: 0px;">
                                            <h4>After</h4>
                                            <div class="content-passage">
</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ft-home-getting-started" id="contact-us">
                    <div class="container">
                        <div class="getting-started-responsive getting-started-text col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <h3 style="margin: px;">Begin Your Fitness Journey Today...</h3>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="ft-discount col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <div class="ft-discount-image">
                            <div class="ft-google-map" data-pgc="om.maps">
                                <div id="map-canvas"></div>
                            </div>
                        </div>
                    </div>
                    <div class="ft-discount col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <div class="container" style="background-color: #f4f4f4; height: 470px;">
                            <div class="contact-form-area">
                                <form method="post" action="assets/inc/sendemail.php" class="contact-form">
                                    <input type="text" name="name" id="name" class="name form-control" placeholder="Name">
                                    <input type="email" name="email" id="email" class="email form-control" placeholder="Email">
                                    <input type="number" name="phon" id="phone" class="phone form-control" placeholder="Phone">
                                    <textarea name="message" class="form-control" placeholder="Message"></textarea>
                                    <div class="ft-contact-button">
                                        <button type="submit" class="btn btn-default">
                                            <span class="text">Submit Now</span>
                                            <span class="icon"><i class="fa fa-arrow-circle-right"></i></span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <footer>
                <div class="footer-wraper">
                    <!-- Newsletter section Starting Point -->
                    <!-- Footer Part Starting section -->
                    <div class="ft-footer">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                                    <div class="footer-left-wrapper">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="ft-footer-left">
                                                    <h5>Get In Touch</h5>
                                                    <ul>
                                                        <li>
                                                            <div class="icon">
                                                                <i class="fa fa-map-marker"></i>
                                                            </div>
                                                            <div class="item">Sanp Fitness, South Central, 22/87 Armadale Rd, Jandakot WA 6164</div>
                                                        </li>
                                                        <li>
                                                            <div class="icon">
                                                                <i class="fa fa-phone"></i>
                                                            </div>
                                                            <div class="item">0413 173 794</div>
                                                        </li>
                                                        <li>
                                                            <div class="icon">
                                                                <i class="fa fa-envelope-o"></i>
                                                            </div>
                                                            <div class="item">
                                                                <a href="hello@omadaactive.com.au">hello@omadaactive.com.au</a>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div class="ft-footer-middle">
                                                    <h5>working Hours</h5>
                                                    <ul>
                                                        <li>
                                                            <div class="left-table">
                                                                Monday - Friday
</div>
                                                            <div class="right-table">
                                                                06.00 - 18.00
</div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="ft-footer-middle">
                                                    <h5 style="margin-top: 25px;">Social Media</h5>
                                                    <div class="footer-copyright-area" style="padding-top: 0px;">
                                                        <div class="row">
                                                            <div class="copyright-left col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                                <div class="social">
                                                                    <a href="https://www.facebook.com/OMADA-Active-1628270034103907" class="img-circle"><span><i class="fa fa-facebook"></i></span></a>
                                                                    <a href="https://www.instagram.com/omadaactive/" class="img-circle"><span><i class="fa fa-instagram"></i></span></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer-copyright-area">
                                        <div class="row">
                                            <div class="copyright-right col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                                <p>copyright &#169; Omada Active 2016. All rights reserved. </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ft-footer-right-bg col-lg-5 col-md-5 col-sm-5 col-xs-12" style="padding: 0px;">
                                    <div class="ft-footer-right">
</div>
                                </div>
                            </div>
                        </div>                         
                    </div>
                </div>
            </footer>             
            <!-- Start Blog Section -->             
        </div>         
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="assets/js/jquery.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Scroling Header JS Area-->
        <script src="assets/js/classie.js"></script>
        <script src="assets/js/cbpAnimatedHeader.js"></script>
        <script src="assets/js/jquery.stellar.js"></script>
        <script src="assets/revolution/js/jquery.themepunch.tools.min.js"></script>         
        <!-- Revolution Slider Tools -->
        <script src="assets/revolution/js/jquery.themepunch.revolution.min.js"></script>         
        <!-- Revolution Slider -->
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <!-- Owl Carosel JS Area -->
        <script src="assets/js/owl.carousel.js"></script>
        <!-- CSS Menu JS Area -->
        <script src="assets/js/css-menu.js"></script>
        <!-- Shuffle Plugin JS Area -->
        <script src="assets/js/jquery.shuffle.min.js"></script>         
        <!-- Comming Soon Countdown JS Area -->
        <script src="assets/js/jquery.countdown.min.js"></script>
        <!-- Counter Section JS Area -->
        <script src="assets/js/jquery.counterup.min.js"></script>
        <script src="assets/js/counter.js"></script>
        <script src="assets/js/waypoints.min.js"></script>
        <!-- mix it js -->
        <script src="assets/js/jquery.mixitup.min.js"></script>
        <!-- Fitness Custom JS Area -->
        <script src="assets/js/custom.js"></script>
        <!-- Maps Custom JS Area -->
        <script src="https://maps.googleapis.com/maps/api/js"></script>
        <script src="assets/js/map.js"></script>
        <script>
function scrollNav() {
  $('.nav a').click(function(){
    //Toggle Class
    $(".active").removeClass("active");
    $(this).closest('li').addClass("active");
    var theClass = $(this).attr("class");
    $('.'+theClass).parent('li').addClass('active');
    //Animate
    $('html, body').stop().animate({
        scrollTop: $( $(this).attr('href') ).offset().top - 160
    }, 400);
    return false;
  });
  $('.scrollTop a').scrollTop();
}
scrollNav();
</script>
    </body>
</html>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
