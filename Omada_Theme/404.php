<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<html>
	<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>FitnessPro | HTML</title>
        <!-- Bootstrap -->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Theme CSS Area -->
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="stylesheet" href="assets/css/color.css">
        <link rel="stylesheet" href="assets/css/css-menu/css-menu.css">
        <link rel="stylesheet" href="assets/css/custom-responsive.css">
        <link rel="stylesheet" href="assets/css/responsive.css">
        <link rel="stylesheet" href="assets/css/hover.css">
        <!-- Fraction Slider CSS Area -->
        <link rel="stylesheet" href="assets/css/fraction-slide/slider-style.css" type="text/css" /> 
        <link rel="stylesheet" href="assets/css/fraction-slide/fractionslider.css" type="text/css" />
        <!-- Owl Carosel CSS Area -->
        <link rel="stylesheet" href="assets/css/owl-carousel/owl.carousel.css" type="text/css" /> 
        <link rel="stylesheet" href="assets/css/owl-carousel/owl.theme.css" type="text/css" />
        <!-- FontAwesome CSS Area -->
        <link href="assets/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>          
        <div class="wrapper ft-main-content">
            <div class="section-header navbar-fixed-top">
                <div class="header-serarch-area">
                    <div class="container">
                        <div class="pull-right search  col-lg-3 col-md-4 col-sm-5 col-xs-12">
                            <form method="post">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn"><button class="btn btn-default" type="button">
                                            <i class="fa fa-search"></i>
                                        </button></span>
                                </div>
                                <!-- /input-group -->
                            </form>                             
                        </div>
                    </div>
                </div>
                <div class="header">
                    <div class="container"> 
                        <div class="row">
                            <!-- Logo Area -->
                            <div class="logo-responsive col-lg-4 col-md-4 col-sm-4 col-xs-12"> 
                                <div class="ft-logo">
                                    <a href="index.html">
                                        <img src="assets/images/omada/OmadaLogo.png" alt="Omada Logo">
                                    </a>
                                </div>
                            </div>                             
                            <!-- Navigation Area -->
                            <div class="menu-responsive col-lg-7 col-md-7 col-sm-7 col-xs-12 pull-right"> 
                                <div class="ft-menu">
                                    <nav id="cssmenu" class="nav">
                                        <ul class="pull-right">
                                            <li>
                                                <a href="index.html#home">Home</a>
                                            </li>
                                            <li>
                                                <a href="index.html#about-us">About Us</a>
                                            </li>
                                            <li>
                                                <a href='index.html#services'>Services</a>
                                            </li>
                                            <li>
                                                <a href='index.html#testimonials'>Testimonials</a>
                                            </li>
                                            <li>
                                                <a href="#">Blog</a>
                                            </li>
                                            <li>
                                                <a href="index.html#contact-us">Contact Us</a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>                                 
                            </div>
                            <!-- Search & Cart Area -->                             
                        </div>                         
                    </div>                     
                </div>
            </div>
            <div class="ft-main-content">
                <div class="section-404">
                    <div class="section-404-bg">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <h1>404</h1>
                                    <h2>Oops!, That page can't be found.</h2>
                                    <p>Sorry, but the page you are looking for does not existing</p>
                                    <a class="btn btn-default" href="index.html">Go to Home Page</a>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ft-newsletter-area">
                <div class="container">
                    <div class="row">
                        <div class="newsletter-text col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="ft-newsletter-des">
                                <h4>Join Our Mailing List</h4>
                                <p>Get Regular Updates Straight To your Inbox</p>
                            </div>
                        </div>
                        <div class="newsletter-content col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <form method="post">
                                <div class="ft-newletter-form-wrap">
                                    <div class="row">
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                            <div class="ft-newletter-form">
                                                <input type="email" name="newletter" id="newletter" class="newletter form-control" placeholder="Enter your email address" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <div class="ft-newletter-button">
                                                <button type="submit" class="btn btn-default">
                                                    <span class="text">Send</span>
                                                    <span class="icon"><i class="fa fa-arrow-circle-right"></i></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>                             
                        </div>
                    </div>
                </div>
            </div>             
            <div class="ft-main-content">
                <!-- ***************************************************************************************************************** -->
                <!-- ********************************************* Home Getting Started Area ***************************************** -->
                <!-- ***************************************************************************************************************** -->                 
                <!-- ***************************************************************************************************************** -->
                <!-- ******************************************** Home Fitness Program Area ***************************************** -->
                <!-- ***************************************************************************************************************** -->                 
                <!-- ***************************************************************************************************************** -->
                <!-- *************************************** Home Fitness Awesome Classes Area *************************************** -->
                <!-- ***************************************************************************************************************** -->
                <!-- ***************************************************************************************************************** -->
                <!-- ******************************************* Home Fitness Discount Area ******************************************* -->
                <!-- ***************************************************************************************************************** -->                 
                <!-- ***************************************************************************************************************** -->
                <!-- ******************************************* Home Fitness Courses Area ******************************************* -->
                <!-- ***************************************************************************************************************** -->                 
                <!-- ***************************************************************************************************************** -->
                <!-- ************************************************** Home Team Area *********************************************** -->
                <!-- ***************************************************************************************************************** -->                 
                <!-- ***************************************************************************************************************** -->
                <!-- ********************************************** Home Testimonial Area ******************************************** -->
                <!-- ***************************************************************************************************************** -->                 
                <!-- ***************************************************************************************************************** -->
                <!-- ********************************************** Home Price Table Area ******************************************** -->
                <!-- ***************************************************************************************************************** -->                 
            </div>
            <!-- Start Blog Section -->             
        </div>
        <div class="footer-wraper">
            <!-- Newsletter section Starting Point -->
            <!-- Footer Part Starting section -->
            <div class="ft-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <div class="footer-left-wrapper">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="ft-footer-left">
                                            <h5>Get In Touch</h5>
                                            <ul>
                                                <li>
                                                    <div class="icon">
                                                        <i class="fa fa-map-marker"></i>
                                                    </div>
                                                    <div class="item">Sanp Fitness, South Central, 22/87 Armadale Rd, Jandakot WA 6164</div>
                                                </li>
                                                <li>
                                                    <div class="icon">
                                                        <i class="fa fa-phone"></i>
                                                    </div>
                                                    <div class="item">0413 173 794</div>
                                                </li>
                                                <li>
                                                    <div class="icon">
                                                        <i class="fa fa-envelope-o"></i>
                                                    </div>
                                                    <div class="item">
                                                        <a href="heloo@omadaactive.com">hello@omadaactive.com</a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="ft-footer-middle">
                                            <h5>working Hours</h5>
                                            <ul>
                                                <li>
                                                    <div class="left-table">
                                                        Monday - Friday
</div>
                                                    <div class="right-table">
                                                        06.00 - 18.00
</div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="ft-footer-middle">
                                            <h5 style="margin-top: 25px;">Social Media</h5>
                                            <div class="footer-copyright-area" style="padding-top: 0px;">
                                                <div class="row">
                                                    <div class="copyright-left col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                                        <div class="social">
                                                            <a href="https://www.facebook.com/OMADA-Active-1628270034103907" class="img-circle"><span><i class="fa fa-facebook"></i></span></a>
                                                            <a href="https://www.instagram.com/omadaactive/" class="img-circle"><span><i class="fa fa-instagram"></i></span></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                             
                            <div class="footer-copyright-area">
                                <div class="row">
                                    <div class="copyright-right col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <p>copyright &#169; Omada Active 2016. All rights reserved. </p>
                                    </div>
                                </div>
                            </div>                             
                        </div>
                        <div class="ft-footer-right-bg col-lg-5 col-md-5 col-sm-5 col-xs-12" style="padding: 0px;">
                            <div class="ft-footer-right">
</div>
                        </div>
                    </div>                     
                </div>                 
            </div>
        </div>         
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="assets/js/jquery.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- Scroling Header JS Area-->
        <script src="assets/js/classie.js"></script>
        <script src="assets/js/cbpAnimatedHeader.js"></script>
        <script src="assets/js/jquery.stellar.js"></script>
        <script src="assets/revolution/js/jquery.themepunch.tools.min.js"></script>         
        <!-- Revolution Slider Tools -->
        <script src="assets/revolution/js/jquery.themepunch.revolution.min.js"></script>         
        <!-- Revolution Slider -->
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="assets/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <!-- Owl Carosel JS Area -->
        <script src="assets/js/owl.carousel.js"></script>
        <!-- CSS Menu JS Area -->
        <script src="assets/js/css-menu.js"></script>
        <!-- Shuffle Plugin JS Area -->
        <script src="assets/js/jquery.shuffle.min.js"></script>         
        <!-- Comming Soon Countdown JS Area -->
        <script src="assets/js/jquery.countdown.min.js"></script>
        <!-- Counter Section JS Area -->
        <script src="assets/js/jquery.counterup.min.js"></script>
        <script src="assets/js/counter.js"></script>
        <script src="assets/js/waypoints.min.js"></script>
        <!-- mix it js -->
        <script src="assets/js/jquery.mixitup.min.js"></script>
        <!-- Fitness Custom JS Area -->
        <script src="assets/js/custom.js"></script>
        <!-- Maps Custom JS Area -->
        <script src="https://maps.googleapis.com/maps/api/js"></script>
        <script src="assets/js/map.js"></script>
        <script>
function scrollNav() {
  $('.nav a').click(function(){  
    //Toggle Class
    $(".active").removeClass("active");      
    $(this).closest('li').addClass("active");
    var theClass = $(this).attr("class");
    $('.'+theClass).parent('li').addClass('active');
    //Animate
    $('html, body').stop().animate({
        scrollTop: $( $(this).attr('href') ).offset().top - 160
    }, 400);
    return false;
  });
  $('.scrollTop a').scrollTop();
}
scrollNav();
</script>
    </body>
	</html>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
